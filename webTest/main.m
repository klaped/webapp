//
//  main.m
//  webTest
//
//  Created by Klaus Pedersen on 01/10/12.
//  Copyright (c) 2012 Klaus Pedersen. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
