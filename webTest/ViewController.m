//
//  ViewController.m
//  webTest
//
//  Created by Klaus Pedersen on 01/10/12.
//  Copyright (c) 2012 Klaus Pedersen. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic)NSArray *reklameUrlString;
@property int reklameNr;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    NSString *mainUrlString = @"http://df-apps.dk";
    self.reklameUrlString = [[NSArray alloc] initWithObjects:@"http://df-apps.dk//wp-content/reklame1/banner-1.jpg", @"http://df-apps.dk//wp-content/reklame1/banner-2.jpg",
                                                                 @"http://df-apps.dk//wp-content/reklame1/banner-3.jpg", @"http://df-apps.dk//wp-content/reklame1/banner-4.jpg",
                                                                 @"http://df-apps.dk//wp-content/reklame1/banner-5.jpg", @"http://df-apps.dk//wp-content/reklame1/banner-6.jpg",
                                                                 @"http://df-apps.dk//wp-content/reklame1/banner-7.jpg", @"http://df-apps.dk//wp-content/reklame1/banner-8.jpg", nil];
    
    NSURL *mainUrl = [NSURL  URLWithString:mainUrlString];
    NSURLRequest *mainRequest = [NSURLRequest requestWithURL:mainUrl];
    self.mainWebView.scalesPageToFit = YES;
    [self.mainWebView loadRequest:mainRequest];
    
    NSURL *reklameUrl = [NSURL URLWithString:[self.reklameUrlString objectAtIndex:0]];
    NSURLRequest *reklameRequest = [NSURLRequest requestWithURL:reklameUrl];
    self.reklameWebView.scrollView.scrollEnabled = NO;
    [self.reklameWebView setUserInteractionEnabled:YES];
    
    [self.reklameWebView loadRequest:reklameRequest];
    self.reklameWebView.delegate = self;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        self.reklameWebView.scalesPageToFit = YES;
        self.mainWebView.scalesPageToFit = YES;
    }

    
    UISwipeGestureRecognizer *swipeGes = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeOnMainPage:)];
    swipeGes.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.mainWebView addGestureRecognizer:swipeGes];
    swipeGes.delegate = self;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)];
    tap.numberOfTapsRequired = 1;
    tap.delegate = self;
    
    [self.reklameWebView addGestureRecognizer:tap];
    
    [self performSelector:@selector(loadNewReklame) withObject:nil afterDelay:6];
}

- (void)loadNewReklame
{
    if (++self.reklameNr > 7)
        self.reklameNr = 0;
    NSLog(@"Loading a new reklame nr:%i", self.reklameNr);
    
    NSURL *reklameUrl = [NSURL URLWithString:[self.reklameUrlString objectAtIndex:self.reklameNr]];
    NSURLRequest *reklameRequest = [NSURLRequest requestWithURL:reklameUrl];
    [self.reklameWebView loadRequest:reklameRequest];
    
    [self performSelector:@selector(loadNewReklame) withObject:nil afterDelay:6];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ( navigationType == UIWebViewNavigationTypeLinkClicked ){
        [[UIApplication sharedApplication] openURL:[request URL]];
    
        NSLog(@"Some one have clicked on a link");
        return NO;
    }
    if (navigationType == UIWebViewNavigationTypeOther)
        NSLog(@"other action");
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    // This scrolls 30 points down
//    [self.reklameWebView stringByEvaluatingJavaScriptFromString:@"window.scrollTo(0,30)"] ;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"We have error:%@", error);
}

- (void) swipeOnMainPage: (UISwipeGestureRecognizer *)sender
{
    NSLog(@"goBack");
    [self.mainWebView goBack];
 
}

- (void) onTap: (UITapGestureRecognizer *)sender
{
    NSLog(@"we have a tap");
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{   
    return YES;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)viewWillLayoutSubviews
{
    NSLog(@"rotate");
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if ( toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight){
            NSLog(@"så ligger vi ned");
            [self.mainWebView setFrame:CGRectMake(0, 0, 1024, 768-100)];
            [self.reklameWebView setFrame:CGRectMake((1024-768)/2, 768-100, 1024, 100)];
        }
        if (toInterfaceOrientation == UIInterfaceOrientationPortrait || toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown){
            NSLog(@"op og stå");
            [self.mainWebView setFrame:CGRectMake(0, 0, 768, 1024-100)];
            [self.reklameWebView setFrame:CGRectMake(0, 1024-100, 768, 100)];
        }
    }
    else
    {
        if ( toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight){
            NSLog(@"så ligger vi ned iPhone");
            [self.mainWebView setFrame:CGRectMake(0, 0, 480, 320-80)];
            [self.reklameWebView setFrame:CGRectMake(0, 320-80, 480, 80)];
            self.reklameWebView.scalesPageToFit = YES;
            self.mainWebView.scalesPageToFit = YES;
        }
        if (toInterfaceOrientation == UIInterfaceOrientationPortrait || toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown){
            NSLog(@"op og stå iPhone");
            [self.mainWebView setFrame:CGRectMake(0, 0, 320, 480-60)];
            [self.reklameWebView setFrame:CGRectMake(0, 480-60, 320, 60)];
            self.reklameWebView.scalesPageToFit = YES;
            self.mainWebView.scalesPageToFit = YES;
        }
    }
    
}



@end
