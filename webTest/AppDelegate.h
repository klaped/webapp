//
//  AppDelegate.h
//  webTest
//
//  Created by Klaus Pedersen on 01/10/12.
//  Copyright (c) 2012 Klaus Pedersen. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
