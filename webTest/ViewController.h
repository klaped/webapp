//
//  ViewController.h
//  webTest
//
//  Created by Klaus Pedersen on 01/10/12.
//  Copyright (c) 2012 Klaus Pedersen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIWebViewDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic)IBOutlet UIWebView *mainWebView;
@property (weak, nonatomic)IBOutlet UIWebView *reklameWebView;

@end
